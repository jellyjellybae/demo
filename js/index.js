//折线图
function lineChart(params) {


  const myChart = echarts.init(document.querySelector('#line'))
  const Options = {
    // 标题配置
    title: {
      text: '2022全学科薪资走势',
      top: 15,
      left: 10,
      textStyle: {
        fontSize: '16'
      },
      left: 'center',
    },
    // "hover" trigger
    tooltip: {
      trigger: 'axis'
    },
    grid: {
      top: '20%'
    },
    // x轴 文字相关:label
    xAxis: {
      type: 'category',
      data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
      axisLabel: {
        color: '#999'
      },
      // 轴线设置
      axisLine: {
        lineStyle: {
          color: '#ccc',
          type: 'dashed'
        }
      },
    },
    // y轴
    yAxis: {
      type: 'value',
      axisLabel: {
        // color: 'lime'
      },
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      }
    },


    // data array
    series: [
      // 线
      {
        // name: 'order count',
        type: 'line',
        data: [9000, 12000, 15000, 13000, 10000, 18000, 14000, 10000, 12000, 13000, 15000, 19000],
        lineStyle: {
          width: 6// 0.1的线条是非常细的了
        },
        // symblo
        symbolSize: '10',//little circle size
        emphasis: {
          scale: 'true',
          focus: 'self'

        },
        smooth: 'true',
        areaStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [
              {
                offset: 0,
                color: '#499FEE' // 0% 处的颜色
              },
              {
                offset: 0.8,
                color: 'rgba(255,255,255,0.2)' // 0% 处的颜色
              },
              {
                offset: 1,
                color: 'rgba(255,255,255,0)' // 100% 处的颜色
              }
            ],
            global: false // 缺省为 false
          }
        },
      },


    ],
    color: {
      type: 'linear',
      x: 0,
      y: 0,
      x2: 0,
      y2: 1,
      colorStops: [{
        offset: 0, color: '#499FEE' // 0% 处的颜色
      }, {
        offset: 1, color: '#5D75F0' // 100% 处的颜色
      }],
      global: false // 缺省为 false
    }

  }
  myChart.setOption(Options)
}
lineChart()
// pie(right top)
function classSalaryChart() {
  const myChart = echarts.init(document.querySelector('#salary'))
  const Options = {
    title: {
      text: '班级薪资分布',
      top: 15,
      left: 10,
      textStyle: {
        fontSize: '16'
      },
      left: 'center',
    },

    tooltip: {
      trigger: 'item'
    },
    legend: {
      left: 'center',
      bottom: '5%'
    },
    color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
    series: [
      {
        name: '班级薪资分布',
        type: 'pie',
        radius: ['50%', '64%'],//r

        itemStyle: {
          borderRadius: 10,
          borderColor: '#fff',
          borderWidth: 2
        },
        label: {
          width: '10',
          show: false,
          // position: 'center'
        },
        labelLine: {
          show: false
        },
        data: [
          { value: 1048, name: '1万以下' },
          { value: 235, name: '1-1.5万' },
          { value: 580, name: '1.5-2万' },
          { value: 484, name: '2万以上' }
        ]
      }
    ]
  }
  myChart.setOption(Options)

}
classSalaryChart()
// bar
function lines() {
  const myChart = echarts.init(document.querySelector('#lines'))
  const Options =  {
    // x轴，y轴，复制折线图（自己修改一下人名）
    xAxis: {
      type: 'category',
      data: ['张三', '李四', '赵云', '张飞', '马超', '黄忠', '亚索', '盲僧'],
      // x轴文字配置
      axisLabel: {
        color: '#999'
      },
      // 轴线设置
      axisLine: {
        // 轴线样式设置
        lineStyle: {
          color: '#ccc',
          type: 'dashed'
        }
      }
    },
    yAxis: {
      type: 'value',
      // Y轴分割线配置
      splitLine: {
        lineStyle: {
          type: 'dashed'
        }
      }
    },
    // 系列数据，需要两份数据，所以复制一个小对象（加入name属性，把数据改改）
    series: [
      {
        data: [12200, 17932, 13901, 13934, 21290, 23300, 13300, 13320],
        type: 'bar',
        name: '期望薪资'
      },
      {
        data: [22820, 19932, 16901, 15934, 31290, 13300, 14300, 18320],
        type: 'bar',
        name: '实际薪资'
      }
    ],
    // 输入移入的提示
    tooltip: {},
    // 颜色设置 color: ['第1个柱子颜色', '第2个柱子颜色']
    color: [
      {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: '#34D39A' // 0% 处的颜色
          },
          {
            offset: 1,
            color: 'rgba(52,211,154,0.2)' // 100% 处的颜色
          }
        ],
        global: false // 缺省为 false
      },
      {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: '#499FEE' // 0% 处的颜色
          },
          {
            offset: 1,
            color: 'rgba(73,159,238,0.2)' // 100% 处的颜色
          }
        ],
        global: false // 缺省为 false
      }
    ],
    // 网格设置（向四外拉伸一下图表）
    grid: {
      top: 30,
      bottom: 50,
      left: 70,
      right: 30
    }
  }
  myChart.setOption(Options)
}
lines()
// pie
function gender() {
  const myChart = echarts.init(document.querySelector('#gender'))
  const Options = {// 标题配置
    title: [
      {
        text: '男女薪资分布',
        left: 10, // 10 表示10px
        top: 10,
        textStyle: {
          fontSize: 16
        }
      },
      {
        text: '男生',
        left: '45%', // 10 表示10px
        top: '45%',
        textStyle: {
          fontSize: 12
        }
      },
      {
        text: '女生',
        left: '45%', // 10 表示10px
        top: '85%',
        textStyle: {
          fontSize: 12
        }
      }
    ],
    // 鼠标移入的提示
    tooltip: {
      trigger: 'item'
    },
    // 颜色的设置
    color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
    // 系列数据
    series: [
      {
        name: '男生薪资分布', // 这个名字会显示在 提示框中
        type: 'pie', // 表示饼图
        center: ['50%', '30%'], // 圆心点坐标
        radius: ['20%', '30%'], // 内外圈的半径
        data: [
          { value: 1048, name: '1万以下' },
          { value: 235, name: '1万-2万' },
          { value: 580, name: '1.5万-2万' },
          { value: 484, name: '2万以上' }
        ]
      },
      {
        name: '男生薪资分布', // 这个名字会显示在 提示框中
        type: 'pie', // 表示饼图
        center: ['50%', '70%'], // 圆心点坐标
        radius: ['20%', '30%'], // 内外圈的半径
        data: [
          { value: 1048, name: '1万以下' },
          { value: 235, name: '1万-2万' },
          { value: 580, name: '1.5万-2万' },
          { value: 484, name: '2万以上' }
        ]
      }
    ]
  }
  myChart.setOption(Options)
}
gender()
// map
// 因为用地图表示每个省有几名同学
// 所以可以提前准备好数据
const mapData = [
  { name: '南海诸岛', value: 0 },
  { name: '北京', value: 3 },
  { name: '天津', value: 2 },
  { name: '上海', value: 4 },
  { name: '重庆', value: 1 },
  { name: '河北', value: 20 },
  { name: '河南', value: 23 },
  { name: '云南', value: 0 },
  { name: '辽宁', value: 15 },
  { name: '黑龙江', value: 12 },
  { name: '湖南', value: 2 },
  { name: '安徽', value: 5 },
  { name: '山东', value: 18 },
  { name: '新疆', value: 0 },
  { name: '江苏', value: 5 },
  { name: '浙江', value: 1 },
  { name: '江西', value: 4 },
  { name: '湖北', value: 3 },
  { name: '广西', value: 2 },
  { name: '甘肃', value: 9 },
  { name: '山西', value: 11 },
  { name: '内蒙古', value: 14 },
  { name: '陕西', value: 14 },
  { name: '吉林', value: 10 },
  { name: '福建', value: 0 },
  { name: '贵州', value: 0 },
  { name: '广东', value: 0 },
  { name: '青海', value: 3 },
  { name: '西藏', value: 0 },
  { name: '四川', value: 1 },
  { name: '宁夏', value: 1 },
  { name: '海南', value: 0 },
  { name: '台湾', value: 0 },
  { name: '香港', value: 0 },
  { name: '澳门', value: 0 }
]

function map() {
  const myChart = echarts.init(document.querySelector('#map'))
  const Options = {
    // 视觉映射组件
    visualMap: {
      type: 'continuous', // continuous表示连续型； piecewise表示分段式
      min: 0,
      max: 20, // 看每个地区的学员多少，再来决定
      inRange: {
        color: ['#fff', '#0075F0']
      },
      text: [20, 0], // 两端的文字
      left: 40,
      bottom: 20
    },
    // 标题配置
    title: {
      text: '籍贯分布',
      top: 15,
      left: 10,
      textStyle: {
        fontSize: 16
      }
    },
    // 肯定需要配置的有：series
    series: [
      {
        name: '籍贯分布', // 添加系列数据的名字（和鼠标移入的提示有关系）
        type: 'map',
        map: 'china', // 具体是什么地图
        // 显示文本标签（每个区域、每个省的名字）
        label: {
          show: true,
          color: 'rgba(0, 0, 0, 0.7)',
          fontSize: 10
        },
        // 每个区域（省）【默认】的样式
        itemStyle: {
          areaColor: '#E0FFFF',
          borderColor: 'rgba(0, 0, 0, 0.2)'
        },
        // 高亮状态，每个区域的配置
        emphasis: {
          // 高亮状态下（鼠标移入状态，或者选中状态下）
          itemStyle: {
            areaColor: '#34D39A',
            shadowBlur: 20,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        },
        // 每个区域的数据
        data: mapData
      }
    ],
    // 鼠标移入的提示
    tooltip: {
      formatter: '{b}：{c}位学员', // {a}是series里面大的name；{b}表示每个区域的名字(省的名字)；{c}表示该地区的值
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      borderColor: 'transparent',
      textStyle: {
        color: '#fff'
      }
    }
  }

  
  myChart.setOption(Options)
}
map()