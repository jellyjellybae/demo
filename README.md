# 数据可视化
## ECHARTS使用方法
引入 Apache ECharts
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <!-- 引入刚刚下载的 ECharts 文件 -->
    <script src="echarts.js"></script>
  </head>
</html>
```
绘图前我们需要为 ECharts 准备一个定义了高宽的 DOM 容器
```html
<body>
  <!-- 为 ECharts 准备一个定义了宽高的 DOM -->
  <div id="main" style="width: 600px;height:400px;"></div>
</body>
```
echarts.init 方法初始化一个 echarts 实例并通过 setOption 方法生成一个简单的柱状图
```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>ECharts</title>
    <!-- 引入刚刚下载的 ECharts 文件 -->
    <script src="echarts.js"></script>
  </head>
  <body>
    <!-- 为 ECharts 准备一个定义了宽高的 DOM -->
    <div id="main" style="width: 600px;height:400px;"></div>
    <script type="text/javascript">
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById('main'));

      // 指定图表的配置项和数据
      var option = {
        title: {
          text: 'ECharts 入门示例'
        },
        tooltip: {},
        legend: {
          data: ['销量']
        },
        xAxis: {
          data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [
          {
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
          }
        ]
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
    </script>
  </body>
</html>
```
配置项中查询配置的使用方法
## Options 常用的配置方法
title - 标题

xAxis - X轴配置

yAxis - Y轴配置

series - 系列数据

格式是 [{ 一个图形 }, { 一个图形 }, { 一个图形 }]
color - 颜色配置

格式 ['red', 'green', 'blue']
格式 ['red', 'green', '渐变色'] ----- [渐变色传送门](https://www.makeapie.cn/doc/echarts/zh/option.html#color)
```js
 color:
  [
    'red',
    {
      type: 'linear',
      x: 0,
      y: 0,
      x2: 0,
      y2: 1,
      colorStops: [{
        offset: 0, color: 'skyblue' // 0% 处的颜色
      }
        ,
      {
        offset: .5, color: 'blue' // 100% 处的颜色
      }
        ,
      {
        offset: 1, color: '#fad' // 100% 处的颜色
      }
      ],
      global: false // 缺省为 false
    },
    'green'
  ]
```

grid - 坐标轴区域的配置，比如配置距离顶部 100 像素

legend - 图例配置

需要设置series中每一项的 name 属性才行。
tooltip - 鼠标移入提示

默认是鼠标移入图形才能提示
将 trigger: 'axis' 之后，鼠标输到轴上即可提示。
```js
  const config = {
      // 标题配置
      title: {
        text: 'my first chart',
        subtext: 'yepyep',
        textStyle: {
          color: `#fad`,
          fontSize: '24'
        },
        left: 'center',
      },
      // x轴 文字相关:label
      xAxis: {
        axisLabel: {
          color: 'brown'
        },
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
      },
      // y轴
      yAxis: {
        type: 'value',
        axisLabel: {
          color: 'lime'
        }
      },

      // data array
      series: [
        // 线
        {
          name: 'order count',
          type: 'line',
          data: [150, 230, 224, 218, 135, 147, 260],
          // color: '#fad',

          lineStyle: {
            width: 5// 0.1的线条是非常细的了
          },
          emphasis: {
            scale: 'true',
            focus: 'self'

          }
        },
        // 柱
        {
          name: 'money',
          type: 'bar',
          data: [50, 50, 60, 10, 45, 34, 30],
          barWidth: "80%",
          // color: '#fad'
        },
        // pie
        // {
        //   data: [50, 50, 60, 10, 45, 34, 30],
        //   type: 'pie'
        // },
      ],
      // type color
      color: [
        '#fad',
        // linear-gradient
        {
          type: 'linear',
          x: 0,
          y: 0,
          x2: 0,
          y2: 1,
          colorStops: [{
            offset: 0, color: 'skyblue' // 0% 处的颜色
          }
            ,
          {
            offset: .5, color: 'blue' // 100% 处的颜色
          }
            ,
          {
            offset: 1, color: '#fad' // 100% 处的颜色
          }
          ],
          global: false // 缺省为 false
        },
        "rgba(0,5,24,.5)",
        "skyblue",
        'yellowgreen',
      ]
      ,//  图例
      legend: {
        top: 60
      },
      // "hover" trigger
      tooltip: {
        trigger: 'axis'
      },
      grid: {
        top: 200
      }
    }
```
### git branch
git log --oneline 显示分支关系